//
//  Constants.swift
//  ReactPad
//
//  Created by Joshua Ide on 2/04/2016.
//  Copyright © 2016 Fox Gallery Studios. All rights reserved.
//

import Foundation

//UserDefaults
let KEY_UID = "uid"
let KEY_BUSINESS = "businessName"
let KEY_IMAGE_PATH = "imagePath"
let KEY_PASSCODE = "passcode"

//Segues
let SEGUE_LOGGED_IN = "loggedIn"
let SEGUE_FIRST_LOGIN = "loggedInFirstTime"
let SEGUE_NOT_LOGGED_IN = "notLoggedIn"
let SEGUE_MAIN_TO_FIRST = "mainToFirstLogin"
let SEGUE_MAIN_TO_PASSCODE = "mainToPasscodeCreator"
let SEGUE_PASSCODE = "toFirstOpenPasscode"
let SEGUE_FINISHED_FIRST_LOGIN = "finishedFirstLogin"

let SEGUE_TO_SURVEYS = "toSurveys"
let SEGUE_TO_ACCOUNTS = "toAccounts"

//Error status codes
let STATUS_ACCOUNT_NONEXIST = -8
let STATUS_PASSWORD_INVALID = -6
let STATUS_EMAIL_INVALID = -5

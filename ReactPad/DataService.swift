//
//  DataService.swift
//  ReactPad
//
//  Created by Joshua Ide on 2/04/2016.
//  Copyright © 2016 Fox Gallery Studios. All rights reserved.
//

import Foundation
import Firebase

let URL_BASE = "https://reactpad.firebaseio.com"

class DataService {
    static let ds = DataService()
    
    private var _REF_BASE = Firebase(url: "\(URL_BASE)")
    private var _REF_USERS = Firebase(url: "\(URL_BASE)/users")
    
    var REF_BASE: Firebase {
        return _REF_BASE
    }
    
    var REF_USERS: Firebase {
        return _REF_USERS
    }
    
    func createFirebaseUser(uid: String, user: Dictionary<String, String>) {
        REF_USERS.childByAppendingPath(uid).setValue(user)
    }
    
    func logoutFirebaseUser() {
        DataService.ds.REF_USERS.childByAppendingPath(KEY_UID).unauth()
        NSUserDefaults.standardUserDefaults().setValue("", forKey: KEY_PASSCODE)
    }
    
    //Profile image
    func saveProfileImage(image: UIImage) {
        let imageData = UIImagePNGRepresentation(image)
        let relativePath = "image\(NSDate.timeIntervalSinceReferenceDate()).png"
        let path = self.documentsPathForFileName(relativePath)
        imageData?.writeToFile(path, atomically: true)
        NSUserDefaults.standardUserDefaults().setObject(relativePath, forKey: KEY_IMAGE_PATH)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func loadProfileImage() -> UIImage {
        var image: UIImage?
        if let imagePath = NSUserDefaults.standardUserDefaults().objectForKey(KEY_IMAGE_PATH) as? String {
            let imagePathFull = self.documentsPathForFileName(imagePath)
            let imageData = NSData(contentsOfFile: imagePathFull)
            image = UIImage(data: imageData!)
        } else {
            image = UIImage(named: "profilepic.png")
        }
        return image!
    }
    
    func documentsPathForFileName(name: String) -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let fullPath = paths[0] as NSString
        return fullPath.stringByAppendingPathComponent(name)
    }
    
}
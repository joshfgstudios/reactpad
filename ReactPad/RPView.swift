//
//  RPView.swift
//  ReactPad
//
//  Created by Joshua Ide on 2/04/2016.
//  Copyright © 2016 Fox Gallery Studios. All rights reserved.
//

import UIKit

@IBDesignable
class RPView: UIView {
    
    @IBInspectable var cornerRoundness: CGFloat = 2
    
    override func drawRect(rect: CGRect) {
        layer.cornerRadius = layer.bounds.width / cornerRoundness
        clipsToBounds = true
    }

}

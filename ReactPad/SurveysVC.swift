//
//  SurveysVC.swift
//  ReactPad
//
//  Created by Joshua Ide on 23/04/2016.
//  Copyright © 2016 Fox Gallery Studios. All rights reserved.
//

import UIKit

class SurveysVC: UIViewController {
    
    //Functions
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    //Actions
    @IBAction func onAddTapped(sender: AnyObject) {
        showErrorAlert("Add Survey", msg: "Here you will be taken to a new VC where you can create your custom survey.")
    }
    
    
    @IBAction func onBackTapped(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }

}

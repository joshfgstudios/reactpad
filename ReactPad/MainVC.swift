//
//  ViewController.swift
//  ReactPad
//
//  Created by Joshua Ide on 30/03/2016.
//  Copyright © 2016 Fox Gallery Studios. All rights reserved.
//

import UIKit

class MainVC: UIViewController {

    //Outlets
    @IBOutlet var tapReports: UITapGestureRecognizer!
    @IBOutlet var tapSurveyMode: UITapGestureRecognizer!
    @IBOutlet var tapSurveys: UITapGestureRecognizer!
    @IBOutlet var tapAccount: UITapGestureRecognizer!
    
    
    //Functions
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(animated: Bool) {
        DataService.ds.REF_BASE.observeAuthEventWithBlock { authData in
            if authData != nil { //user authenticated
                let uid = authData.uid
                NSUserDefaults.standardUserDefaults().setValue(uid, forKey: KEY_UID)
                
                //check if the user ever entered their business name
                DataService.ds.REF_USERS.childByAppendingPath("\(NSUserDefaults.standardUserDefaults().valueForKey(KEY_UID)!)").childByAppendingPath(KEY_BUSINESS).observeSingleEventOfType(.Value, withBlock: { snapshot in
            
                    if snapshot.value is NSNull {
                        self.performSegueWithIdentifier(SEGUE_MAIN_TO_FIRST, sender: nil)
                    } else {
                        //check if user has created a passcode
                        DataService.ds.REF_USERS.childByAppendingPath("\(NSUserDefaults.standardUserDefaults().valueForKey(KEY_UID)!)").childByAppendingPath(KEY_PASSCODE).observeSingleEventOfType(.Value, withBlock: { snapshot in
                            if snapshot.value is NSNull {
                                self.performSegueWithIdentifier(SEGUE_MAIN_TO_PASSCODE, sender: nil)
                            } else {
                                NSUserDefaults.standardUserDefaults().setValue(snapshot.value, forKey: KEY_PASSCODE)
                                return
                            }
                        })
                    }
                })
                return
                
            } else {
                self.performSegueWithIdentifier(SEGUE_NOT_LOGGED_IN, sender: nil)
            }
        }
    }

    //Actions
    @IBAction func onReportsTapped(sender: AnyObject) {
        showErrorAlert("Take me to reports", msg: "From this screen, you will be able to view previous reports and generate new ones.")
    }
    
    @IBAction func onSurveyModeTapped(sender: AnyObject) {
        showErrorAlert("No survey selected", msg: "You don't have any surveys selected yet!  Go to the 'Survey' screen to create a new survey.")
    }
    
    @IBAction func onAccountsTapped(sender: AnyObject) {
        //LOGOUT STRAIGHT FROM HERE UNTIL ACCOUNTS PAGE IS CREATED
        DataService.ds.logoutFirebaseUser()
    }
}


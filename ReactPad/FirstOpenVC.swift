//
//  FirstOpenVC.swift
//  ReactPad
//
//  Created by Joshua Ide on 2/04/2016.
//  Copyright © 2016 Fox Gallery Studios. All rights reserved.
//

import UIKit
import Firebase

class FirstOpenVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    //Outlets
    @IBOutlet weak var txtBusiness: UITextField!
    @IBOutlet weak var imgBusinessProfile: UIImageView!
    
    //Properties
    var imagePicker: UIImagePickerController!
    
    //Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgBusinessProfile.layer.cornerRadius = imgBusinessProfile.frame.size.width / 1.6
        imgBusinessProfile.clipsToBounds = true
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        imgBusinessProfile.image = image
    }
    
    //Actions
    @IBAction func nextTapped(sender: AnyObject) {
        if let businessName = txtBusiness.text where txtBusiness.text != "" {
            DataService.ds.REF_USERS.childByAppendingPath("\(NSUserDefaults.standardUserDefaults().valueForKey(KEY_UID)!)").childByAppendingPath(KEY_BUSINESS).setValue(businessName)
            NSUserDefaults.standardUserDefaults().setValue(txtBusiness.text, forKey: KEY_BUSINESS)
            DataService.ds.saveProfileImage(imgBusinessProfile.image!)
            
            performSegueWithIdentifier(SEGUE_PASSCODE, sender: nil)
        } else {
            showErrorAlert("Business name empty", msg: "You have not entered any business name.  Please enter your business name to continue.")
        }
    }
    
    @IBAction func onUploadLogoTapped(sender: AnyObject) {
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
}

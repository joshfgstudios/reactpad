//
//  SignInVC.swift
//  ReactPad
//
//  Created by Joshua Ide on 2/04/2016.
//  Copyright © 2016 Fox Gallery Studios. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Google

class SignInVC: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {

    //Outlets
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    
    //Functions
    override func viewDidLoad() {
        super.viewDidLoad()

        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    //----GOOGLE SIGNIN----
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!, withError error: NSError!) {
        if error != nil {
            self.showErrorAlert("Google login failed", msg: "An error occured with Google login.  Error: \(error.localizedDescription)")
        } else {
            DataService.ds.REF_BASE.authWithOAuthProvider("google", token: user.authentication.accessToken, withCompletionBlock: { error, authData in
                if error != nil {
                    self.showErrorAlert("Authentication failed", msg: "There has been an authentication problem with Google, and you have not been logged in.")
                } else {
                    //check if user existed already
                    DataService.ds.REF_USERS.childByAppendingPath(authData.uid).observeSingleEventOfType(.Value, withBlock: { snap in
                        //if there's no user
                        if snap.value is NSNull {
                            //Create the user on Firebase
                            let user = ["provider": authData.provider!]
                            DataService.ds.createFirebaseUser(authData.uid, user: user)
                            
                            //Save the UID to NSUserDefaults
                            NSUserDefaults.standardUserDefaults().setValue(authData.uid, forKey: KEY_UID)
                            self.performSegueWithIdentifier(SEGUE_FIRST_LOGIN, sender: nil)
                        } else {
                            //Logged in before, take us straight to main, create NSUserDefault just in case
                            NSUserDefaults.standardUserDefaults().setValue(authData.uid, forKey: KEY_UID)
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }
                    })
                }
                })
        }
    }

    //Actions
    //----FACEBOOK SIGNIN----
    @IBAction func facebookLoginTapped(sender: UIButton!) {
        let facebookLogin = FBSDKLoginManager()
        
        facebookLogin.logInWithReadPermissions(["email"], fromViewController: self) { (facebookResult: FBSDKLoginManagerLoginResult!, facebookError: NSError!) in
            if facebookError != nil {
                self.showErrorAlert("Facebook login failed", msg: "An error occured with Facebook login.  Error: \(facebookError)")
            } else if facebookResult.isCancelled {
                self.showErrorAlert("Facebook login cancelled", msg: "You cancelled the login before authentication was completed.")
                return
            } else {
                let accessToken = FBSDKAccessToken.currentAccessToken().tokenString
                
                DataService.ds.REF_BASE.authWithOAuthProvider("facebook", token: accessToken, withCompletionBlock: { error, authData in
                    if error != nil {
                        self.showErrorAlert("Authentication failed", msg: "There has been an authentication problem with Facebook, and you have not been logged in.")
                    } else {
                        //check if user existed already
                        DataService.ds.REF_USERS.childByAppendingPath(authData.uid).observeSingleEventOfType(.Value, withBlock: { snap in
                            //if there's no user
                            if snap.value is NSNull {
                                //Create the user on Firebase
                                let user = ["provider": authData.provider!]
                                
                                DataService.ds.createFirebaseUser(authData.uid, user: user)
                                
                                //Save the UID to NSUserDefaults
                                NSUserDefaults.standardUserDefaults().setValue(authData.uid, forKey: KEY_UID)
                                self.performSegueWithIdentifier(SEGUE_FIRST_LOGIN, sender: nil)
                            //user already existed
                            } else {
                                //We've logged in before, take us straight to main, and save the UserDefault just in case
                                NSUserDefaults.standardUserDefaults().setValue(authData.uid, forKey: KEY_UID)
                                self.dismissViewControllerAnimated(true, completion: nil)
                            }
                        })
                    }
                })
            }
        }
        
    }
    
    @IBAction func googleLoginTapped(sender: AnyObject) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func emailLoginTapped(sender: AnyObject) {
        if let email = txtEmail.text where txtEmail.text != "", let password = txtPassword.text where txtPassword.text != "" {
            DataService.ds.REF_BASE.authUser(email, password: password, withCompletionBlock: { error, authData in
                if error != nil {
                    //Check for what the specific problem is
                    if error.code == STATUS_EMAIL_INVALID {
                        self.showErrorAlert("Email invalid", msg: "Your email address was invalid.  Please use a valid email address.")
                    } else if error.code == STATUS_PASSWORD_INVALID {
                        self.showErrorAlert("Invalid credentials", msg: "Your password was incorrect.  Please try again.")
                    } else if error.code == STATUS_ACCOUNT_NONEXIST {
                        //Account doesn't exist, create one
                        DataService.ds.REF_BASE.createUser(email, password: password, withValueCompletionBlock: { error, result in
                            if error != nil {
                                self.showErrorAlert("Could not create account", msg: "Something went wrong and your account could not be created.  Please try again.")
                            } else {
                                NSUserDefaults.standardUserDefaults().setValue(result[KEY_UID], forKey: KEY_UID)
                                DataService.ds.REF_BASE.authUser(email, password: password, withCompletionBlock: { err, authData in
                                    //Create the user on Firebase
                                    let user = ["provider": authData.provider!]
                                    DataService.ds.createFirebaseUser(authData.uid, user: user)
                                })
                                
                                self.performSegueWithIdentifier(SEGUE_FIRST_LOGIN, sender: nil)
                            }
                        })
                    }
                } else {
                    //We've logged in before so take us to Main, save NSUserDefault UID just in case
                    NSUserDefaults.standardUserDefaults().setValue(authData.uid, forKey: KEY_UID)
                    self.dismissViewControllerAnimated(true, completion: nil)
                }
            })
        } else {
            showErrorAlert("Empty fields", msg: "Please ensure you enter an email and password.")
        }
    }
    
    
    

}

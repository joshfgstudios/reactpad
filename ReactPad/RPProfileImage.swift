//
//  RPProfileImage.swift
//  ReactPad
//
//  Created by Joshua Ide on 14/04/2016.
//  Copyright © 2016 Fox Gallery Studios. All rights reserved.
//

import UIKit

class RPProfileImage: UIImageView {
    
    override func awakeFromNib() {
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
    }

}

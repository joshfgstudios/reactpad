//
//  FirstOpenPasscodeVC.swift
//  ReactPad
//
//  Created by Joshua Ide on 2/04/2016.
//  Copyright © 2016 Fox Gallery Studios. All rights reserved.
//

import UIKit

class FirstOpenPasscodeVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var lblPasscode: UILabel!
    
    //Properties
    var passcode = ""
    
    //Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //Actions
    @IBAction func numberPressed(button: UIButton!) {
        if passcode.characters.count <= 3 {
            passcode += "\(button.tag)"
            lblPasscode.text = passcode
        }
    }

    @IBAction func clearPressed(sender: AnyObject) {
        passcode = ""
        lblPasscode.text = passcode
    }
    
    @IBAction func onDonePressed(sender: AnyObject) {
        if passcode.characters.count != 4 {
            showErrorAlert("Please complete passcode", msg: "Please ensure you enter a four digit passcode.")
        } else {
            performSegueWithIdentifier(SEGUE_FINISHED_FIRST_LOGIN, sender: nil)
            DataService.ds.REF_USERS.childByAppendingPath("\(NSUserDefaults.standardUserDefaults().valueForKey(KEY_UID)!)").childByAppendingPath(KEY_PASSCODE).setValue(passcode)
            NSUserDefaults.standardUserDefaults().setValue(passcode, forKey: KEY_PASSCODE)
        }
        
    }
    
}

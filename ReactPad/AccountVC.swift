//
//  AccountVC.swift
//  ReactPad
//
//  Created by Joshua Ide on 25/04/2016.
//  Copyright © 2016 Fox Gallery Studios. All rights reserved.
//

import UIKit

class AccountVC: UIViewController {

    //Outlets
    @IBOutlet weak var imgProfilePic: UIImageView!
    
    //Functions
    override func viewDidLoad() {
        super.viewDidLoad()

        imgProfilePic.image = DataService.ds.loadProfileImage()
    }
    
    //Actions
    @IBAction func onBackTapped(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onLogoutTapped(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }

}
